#include "utils.h"

using namespace std;

Resultados Savings(Grafo &g) {
    Resultados res;
    vector<Ahorro> ahorros = calcularAhorros(g);
    sort(ahorros.begin(), ahorros.end(), ordAhorros);
    res.rutas = calcularRutas(ahorros, g);
    res.cantidadDeVehiculos = res.rutas.size();
    res.funcionObjetivo = funcionObj(res.rutas, g);
    return res;
}

//FUNCIONES

vector<Ahorro> calcularAhorros(Grafo g) {
    vector<Ahorro> res;
    for (int i = 0; i < g.CantidadDeElementos(); ++i) {
        for (int j = i; j < g.CantidadDeElementos(); ++j) {
            if (i != g.Deposito() && j != g.Deposito()) {
                if (i != j) {
                    Ahorro ahorro;
                    ahorro.ahorro = g.MatrizDeAdyacencia()[g.Deposito()][i] + g.MatrizDeAdyacencia()[g.Deposito()][j] -
                                    g.MatrizDeAdyacencia()[i][j];
                    ahorro.desde = i;
                    ahorro.hasta = j;
                    res.push_back(ahorro);
                }
            }
        }
    }
    return res;
}

bool ordAhorros(Ahorro a, Ahorro b) {
    return a.ahorro > b.ahorro;
}

vector<ruta> calcularRutas(vector<Ahorro> ahorros, Grafo g) {
    vector<ruta> rutas;
    vectorDeMarcas pasePor(g.CantidadDeElementos(), false);
    int contador = 0;
    for (auto &ahorro : ahorros) {
        if (!(pasePor[ahorro.hasta] ||
              pasePor[ahorro.desde])) { //Si no pase por ningun, me armo una nueva ruta y la agrego al vector de rutas
            if (g.CostoDePasarPorElNodo(ahorro.hasta) + g.CostoDePasarPorElNodo(ahorro.desde) <=
                g.CapacidadDelVehiculo()) {//Veo si el costo de pasar por los nodos entre en mi vehiculo
                ruta rutaNueva;
                rutaNueva.push_back(ahorro.hasta);
                rutaNueva.push_back(ahorro.desde);
                //Marco los nodos como que ya pase por ellos
                pasePor[ahorro.hasta] = true;
                pasePor[ahorro.desde] = true;
                rutas.push_back(rutaNueva);
                contador += 2; //Sumo que ta pase por dos nodos nuevos
            }
        } else if (!pasePor[ahorro.hasta] && pasePor[ahorro.desde]) {//Si uno solo esta usado
            //Busco donde fue usado, si el nuevo entra en mi ruta y si es interior
            for (auto &ruta : rutas) {
                if (costoDeRuta(ruta, g) + g.CostoDePasarPorElNodo(ahorro.hasta) <= g.CapacidadDelVehiculo()) {
                    if (ahorro.desde == ruta[0]) {
                        ruta.insert(ruta.begin(), ahorro.hasta);
                        pasePor[ahorro.hasta] = true;
                        contador++;
                    } else if (ahorro.desde == ruta[ruta.size() - 1]) {
                        ruta.push_back(ahorro.hasta);
                        pasePor[ahorro.hasta] = true;
                        contador++;
                    }
                }
            }
        } else if (!pasePor[ahorro.desde] && pasePor[ahorro.hasta]) {
            for (auto &ruta : rutas) {
                if (costoDeRuta(ruta, g) + g.CostoDePasarPorElNodo(ahorro.desde) <= g.CapacidadDelVehiculo()) {
                    if (ahorro.hasta == ruta[0]) {
                        ruta.insert(ruta.begin(), ahorro.desde);
                        pasePor[ahorro.desde] = true;
                        contador++;
                    } else if (ahorro.hasta == ruta[ruta.size() - 1]) {
                        ruta.push_back(ahorro.desde);
                        pasePor[ahorro.desde] = true;
                        contador++;
                    }
                }
            }
        }//Si los dos son de rutas diferentes
        if (pasePor[ahorro.hasta] && pasePor[ahorro.desde]) {
            //Busco en que rutas estan
            for (int j = 0; j < rutas.size(); ++j) {
                for (int k = 0; k < rutas.size(); ++k) {
                    if (j != k && costoDeRuta(rutas[j], g) + costoDeRuta(rutas[k], g) <=
                                  g.CapacidadDelVehiculo()) { //Si estan en rutas distintas y el costo de esas rutas puede ser llevado a cabo por un camion
                        //Veo que no sean interiores y uno las rutas
                        if (ahorro.hasta == rutas[j][0] ||
                            ahorro.hasta == rutas[j][rutas[j].size() - 1]) {
                            if (ahorro.desde == rutas[k][0] ||
                                ahorro.desde == rutas[k][rutas[k].size() - 1]) {
                                unirRutas(rutas, j, k, ahorro);
                            }
                        }
                    }
                }
            }
        }
        /*
        if(contador == g.CantidadDeElementos()){
            break;
        }*/
    }
    //Sumo uno a los nodos de las rutas
    sumarUno(rutas);
    //Agrego el deposito a las rutas
    agregarDeposito(rutas, g);
    return rutas;
}

void unirRutas(vector<ruta> &rutas, int j, int k, Ahorro ahorro) {
    if (ahorro.hasta == rutas[k][0] && ahorro.desde == rutas[j][0]) {
        for (int i = 0; i < rutas[k].size(); ++i) {
            rutas[j].insert(rutas[j].begin(), rutas[k][i]);
        }
        rutas.erase(rutas.begin() + k);
    }
    if (ahorro.hasta == rutas[k][0] && ahorro.desde == rutas[j][rutas[j].size() - 1]) {
        for (int i = 0; i < rutas[k].size(); ++i) {
            rutas[j].push_back(rutas[k][i]);
        }
        rutas.erase(rutas.begin() + k);
    }
    if (ahorro.hasta == rutas[k][rutas[k].size() - 1] && ahorro.desde == rutas[j][0]) {
        for (int i = 0; i < rutas[j].size(); ++i) {
            rutas[k].push_back(rutas[j][i]);
        }
        rutas.erase(rutas.begin() + j);
    }
    if (ahorro.hasta == rutas[k][rutas[k].size() - 1] && ahorro.desde == rutas[j][rutas[j].size() - 1]) {
        for (int i = rutas[k].size(); i > -1; --i) {
            rutas[j].push_back(rutas[k][i]);
        }
        rutas.erase(rutas.begin() + k);
    }
}

int costoDeRuta(const ruta &ruta, Grafo g) {
    int suma = 0;
    for (int i : ruta) {
        suma += g._costoDelPunto[i];
    }
    return suma;
}

void sumarUno(vector<ruta> &rutas) {
    for (auto &ruta : rutas) {
        for (auto &nodo : ruta) {
            nodo++;
        }
    }
}


void agregarDeposito(vector<ruta> &rutas, Grafo g) {
    for (auto &ruta : rutas) {
        ruta.insert(ruta.begin(), g.Deposito()+1);
        ruta.push_back(g.Deposito()+1);
    }
}

float funcionObj(const vector<ruta> &rutas, Grafo g) {
    float suma = 0;
    for (ruta ruta : rutas) {
        for (int i = 0; i < ruta.size()-1; ++i) {
            suma += g.MatrizDeAdyacencia()[ruta[i]-1][ruta[i+1]-1];
        }
    }
    return suma;
}
/*
#include "utils.h"

using namespace std;

Resultados Savings(Grafo &g){
    Resultados res
    vector<Ahorro> ahorros = calcularAhorros(g);
    sort(ahorros.begin(),ahorros.end(),ordAhorros);
    res.rutas = calcularRutas(ahorros, g);
    res.cantidadDeVehiculos = res.rutas.size();
    res.funcionObjetivo = funcionObj(res.rutas, g);
    return res;
}

//FUNCIONES

vector<Ahorro> calcularAhorros(Grafo g){
    vector<Ahorro> res;
    for (int i = 0; i < g.CantidadDeElementos(); ++i) {
        for (int j = i; j < g.CantidadDeElementos(); ++j) {
            if(i != g.Deposito() && j != g.Deposito()){
                Ahorro ahorro{};
                ahorro.ahorro = g.MatrizDeAdyacencia()[g.Deposito()][i] + g.MatrizDeAdyacencia()[g.Deposito()][j] - g.MatrizDeAdyacencia()[i][j];
                ahorro.desde = i;
                ahorro.hasta = j;
                res.push_back(ahorro);
            }
        }
    }
    return res;
}

bool ordAhorros(Ahorro a, Ahorro b) {
    return a.ahorro > b.ahorro;
}

vector<ruta> calcularRutas(vector<Ahorro> ahorros, Grafo g){
    vector<ruta> rutas;
    vectorDeMarcas pasePor(g.CantidadDeElementos(),false);
    int contador = 0;
    for (auto & ahorro : ahorros) {
        if (!(pasePor[ahorro.hasta] || pasePor[ahorro.desde])) { //Si no pase por ningun, me armo una nueva ruta y la agrego al vector de rutas
            if (g.CostoDePasarPorElNodo(ahorro.hasta) + g.CostoDePasarPorElNodo(ahorro.desde) <= g.CapacidadDelVehiculo()) {//Veo si el costo de pasar por los nodos entre en mi vehiculo
                ruta rutaNueva;
                rutaNueva.push_back(ahorro.hasta);
                rutaNueva.push_back(ahorro.desde);
                //Marco los nodos como que ya pase por ellos
                pasePor[ahorro.hasta] = true;
                pasePor[ahorro.desde] = true;
                rutas.push_back(rutaNueva);
                contador += 2; //Sumo que ta pase por dos nodos nuevos
            }
        } else if (!pasePor[ahorro.hasta] && pasePor[ahorro.desde]) {//Si uno solo esta usado
            //Busco donde fue usado, si el nuevo entra en mi ruta y si es interior
            for (auto & ruta : rutas) {
                if(costoDeRuta(ruta) + g.CostoDePasarPorElNodo(ahorro.hasta) <= g.CapacidadDelVehiculo()){
                    if (ahorro.desde == ruta[0]) {
                        ruta.insert(ruta.begin(), ahorro.hasta);
                    } else if (ahorro.desde == ruta[ruta.size() - 1]) {
                        ruta.push_back(ahorro.hasta);
                    }
                }
            }
            //Acualizo los valores
            pasePor[ahorro.hasta] = true;
            contador++;
        } else if (!pasePor[ahorro.desde] && pasePor[ahorro.hasta]) {
            for (auto & ruta : rutas) {
                if(costoDeRuta(ruta) + g.CostoDePasarPorElNodo(ahorro.desde) <= g.CapacidadDelVehiculo()){
                    if (ahorro.hasta == ruta[0]) {
                        ruta.insert(ruta.begin(), ahorro.desde);
                    } else if (ahorro.hasta == ruta[ruta.size() - 1]) {
                        ruta.push_back(ahorro.desde);
                    }
                }
            }
            pasePor[ahorro.desde] = true;
            contador++;
        }//Si los dos son de rutas diferentes
        if (pasePor[ahorro.hasta] && pasePor[ahorro.desde]) {
            //Busco en que rutas estan
            for (int j = 0; j < rutas.size(); ++j) {
                for (int k = 0; k < rutas.size(); ++k) {
                    if (j != k && costoDeRuta(rutas[j]) + costoDeRuta(rutas[k]) <= g.CapacidadDelVehiculo()) { //Si estan en rutas distintas y el costo de esas rutas puede ser llevado a cabo por un camion
                        //Veo que no sean interiores y uno las rutas
                        if (ahorro.hasta == rutas[j][0] ||
                            ahorro.hasta == rutas[j][rutas[j].size() - 1]) {
                            if (ahorro.desde == rutas[k][0] ||
                                ahorro.desde == rutas[k][rutas[k].size() - 1]) {
                                unirRutas(rutas, j, k, ahorro);
                            }
                        }
                    }
                }
            }
        }
        if(contador == g.CantidadDeElementos()){
            break;
        }
    }
    //Sumo uno a los nodos de las rutas
    sumarUno(rutas);
    //Agrego el deposito a las rutas
    agregarDeposito(rutas, g);
    return rutas;
}

void unirRutas(vector<ruta> rutas, int j, int k, Ahorro &ahorro) {
    if(ahorro.hasta == rutas[k][0] && ahorro.desde == rutas[j][0]){
        for (int i = 0; i < rutas[k].size(); ++i) {
            rutas[j].insert(rutas[j].begin(),rutas[k][i]);
        }
        rutas.erase(rutas.begin()+k);
    }
    if(ahorro.hasta == rutas[k][0] && ahorro.desde == rutas[j][rutas[j].size()-1]){
        for (int i = 0; i < rutas[k].size(); ++i) {
            rutas[j].push_back(rutas[k][i]);
        }
        rutas.erase(rutas.begin()+k);
    }
    if(ahorro.hasta == rutas[k][rutas[k].size()-1] && ahorro.desde == rutas[j][0]){
        for (int i = 0; i < rutas[j].size(); ++i) {
            rutas[k].push_back(rutas[j][i]);
        }
        rutas.erase(rutas.begin()+j);
    }
    if(ahorro.hasta == rutas[k][rutas[k].size()-1] && ahorro.desde == rutas[j][rutas[j].size()-1]){
        for (int i = rutas[k].size(); i > -1 ; --i) {
            rutas[j].push_back(rutas[k][i]);
        }
        rutas.erase(rutas.begin()+k);
    }
}

int costoDeRuta(const ruta& ruta){
    int suma = 0;
    for (int i : ruta) {
        suma += i;
    }
    return suma;
}

void sumarUno(vector<ruta> &rutas){
    for (auto & ruta : rutas) {
        for (auto & nodo : ruta) {
            nodo++;
        }
    }
}

void agregarDeposito(vector<ruta> rutas, Grafo g){
    for (auto & ruta : rutas) {
        ruta.insert(ruta.begin(), g.Deposito());
        ruta.push_back(g.Deposito());
    }
}

float funcionObj(const vector<ruta>& rutas, Grafo g){
    float suma = 0;
    for (ruta ruta : rutas) {
        for (int nodo : ruta) {
            suma += g.MatrizDeAdyacencia()[ruta[nodo]-1][ruta[nodo+1]-1];
        }
    }
    return suma;
}*/
