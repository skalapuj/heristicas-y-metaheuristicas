#include "utils.h"
#include <chrono>

using namespace std;

int main() {
	Grafo g ;
	Resultados res;
	res = Savings(g);
	cout << "Funcion objetivo:" << res.funcionObjetivo << " " << endl;
    cout << "Cantidad de vehiculos:" << res.cantidadDeVehiculos << "" << endl;
    for (int i = 0; i < res.cantidadDeVehiculos; ++i) {
        cout << "Ruta" << " "<< i+1 << ":"<< " ";
        for (int j = 0; j < res.rutas[i].size(); ++j) {
            cout << res.rutas[i][j] << " ";
        }
        cout << endl;
    }
	return 0;
}
