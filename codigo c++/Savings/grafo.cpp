#include "utils.h"

using namespace std;

Grafo::Grafo() {
  cin >> _cantidadDePuntos;
  cin >> _capacidad;
  coord vacio;
  vector<coord> puntosEnElMapa(_cantidadDePuntos, vacio);
    for (int j = 0; j < _cantidadDePuntos ; ++j) {
        coord CordenadaNueva;
        int elementoIesimo;
        cin >> elementoIesimo;
        cin >> CordenadaNueva.x;
        cin >> CordenadaNueva.y;
        puntosEnElMapa[elementoIesimo - 1] = CordenadaNueva;
    };
    _puntosEnElMapa=puntosEnElMapa;

    vector<int> vectorDeCosto(_cantidadDePuntos,0);
    for (int k = 0; k < _cantidadDePuntos; ++k) {
        int elemento;
        cin >> elemento;
        cin >> vectorDeCosto[elemento - 1];
    }
    _costoDelPunto=vectorDeCosto;

    cin >> _indexDeposito;
    _deposito=puntosEnElMapa[_indexDeposito-1];
    for( int i = 0; i < _puntosEnElMapa.size(); i++) {
        vector<float> distancias;
        for( int j = 0; j < _puntosEnElMapa.size(); j++) {

            float distancia;

            float dist = pow(_puntosEnElMapa[i].x - _puntosEnElMapa[j].x,2) + pow(_puntosEnElMapa[i].y - _puntosEnElMapa[j].y,2);
            distancia = sqrt(dist);

            distancias.push_back(distancia);

            arista a;
            a.A = i + 1;
            a.B = j + 1;
            a.peso = distancia;
            _aristas.push_back(a);
        }
        _grafo.push_back(distancias);
    }
}

int Grafo::CapacidadDelVehiculo() {
  return _capacidad;
}
int Grafo::CantidadDeElementos() {
  return _cantidadDePuntos;
}
int Grafo::Deposito(){
  return _indexDeposito-1;
}
vector<vector<float>> Grafo::MatrizDeAdyacencia() {
  return _grafo;
}
int Grafo::CostoDePasarPorElNodo(int i) {
  return _costoDelPunto[i];
}
vector<int> Grafo::CostoPorPunto(){
  return _costoDelPunto;
}
/*
string Grafo::NombreDeInstancia() {
  return _nombre;
}
*/
vector<coord> Grafo::puntosEnElMapa() {
  return _puntosEnElMapa;
}
void Grafo::agregarDeposito(){
  //AGREGAR EL DEPOSITO EN: *CANTIDAD DE PUNTOS *PUNTOS EN EL MAPA *GRAFO
  _cantidadDePuntos++;
  _puntosEnElMapa.insert(_puntosEnElMapa.begin(), _deposito);

  for (int i = 0; i < _grafo.size(); i++) {
    _grafo[i].insert(_grafo[i].begin(), _distanciasAlDeposito[i]);
  }
  _grafo.insert(_grafo.begin(), _distanciasAlDeposito);
  _grafo[0].insert(_grafo[0].begin(), 0);

  return;
}