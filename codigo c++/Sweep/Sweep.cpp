#include "utils.h"

using namespace std;

Resultados Sweep(Grafo &g) {
    //g.agregarDeposito();
    Resultados res;
    clusters losClusters = ClusterSweep(g);
    res.cantidadDeVehiculos = losClusters.size();
    res.funcionObjetivo = 0;
    for (int i = 0; i < losClusters.size(); ++i) {
        TSP(losClusters[i], res, g);
    }
    return res;
}

//FUNCIONES

bool ord(coordP a, coordP b) {
    if (a.t < b.t) {
        return true;
    } else if (a.t == b.t && a.r < b.r) {
        return true;
    } else return false;
}

bool ordK(Adyacencias a, Adyacencias b) {
    return a.peso < b.peso;
}

vector<coordP> CoordenadasPolares(Grafo &grafo) {
    vector<coordP> res;
    Grafo g = grafo;
    int coordenadaXdelDepot = g._puntosEnElMapa[g.Deposito()].x;
    int coordenadaYdelDepot = g._puntosEnElMapa[g.Deposito()].y;
    for (int i = 0; i < g._puntosEnElMapa.size(); ++i) { // muevo el eje al deposito
        g._puntosEnElMapa[i].x = g._puntosEnElMapa[i].x - coordenadaXdelDepot;
        g._puntosEnElMapa[i].y = g._puntosEnElMapa[i].y - coordenadaYdelDepot;
    }
    for (int j = 0; j < g._puntosEnElMapa.size(); ++j) { // paso las coordenadas a coordenadas polares
        int x = g._puntosEnElMapa[j].x;
        int y = g._puntosEnElMapa[j].y;
        coordP pol;
        pol.id = j;
        if (x == 0 && y == 0) { // x= tita en primer cuadrante
            pol.t = 0;
        }
        if (x > 0 && y >= 0) { // x= tita en primer cuadrante
            pol.t = atan(y / x);
        }
        if (x == 0 && y > 0) { //Estoy en x=0 e y mayor a 0
            pol.t = 1.56;
        }
        if (x < 0) { //x= tita en  segundo y tercer cuadrante
            pol.t = atan(y / x) + 3.14;
        }
        if (x == 0 && y < 0) { // x es 0 e y menor que 0
            pol.t = 1.56 + 3.14;
        }
        if (x > 0 && y < 0) {  // x= tita en cuarto cuadrante
            pol.t = atan(y / x) + 6.2814;
        }
        pol.r = sqrt(pow(x, 2) + pow(y, 2)); // x=r
        res.push_back(pol);
    }
    sort(res.begin(), res.end(), ord);
    return res;
}

clusters ClusterSweep(Grafo g) {
    clusters losClusters;
    vector<coordP> coordPol = CoordenadasPolares(g);
    float tita = 0;
    int control = 0;
    vector<int> clusterActual;
    int capacidadActual = g.CapacidadDelVehiculo();
    while (control < g.CantidadDeElementos()) {
        if (coordPol[control].id == g.Deposito()) {
            control++;
            continue;
        } else {
            if (g.CostoPorPunto()[coordPol[control].id] <= capacidadActual) {
                clusterActual.push_back(coordPol[control].id);
                capacidadActual = capacidadActual - g.CostoPorPunto()[coordPol[control].id];
                control++;
                continue;
            } else {
                losClusters.push_back(clusterActual);
                vector<int> nuevoCluster;
                clusterActual = nuevoCluster;
                capacidadActual = g.CapacidadDelVehiculo();
                continue;
            }
        }
    }
    losClusters.push_back(clusterActual); // pusheeo el ultimo cluster
// por cuestiones de redondeo no pueden haber quedado puntos a fuera pues pese que que que no cubro [0,2pi)
// pues los puntos del 4to cuadrante, que podrian ser los que queden afuera cuando defino uso 6.2814 - atan(y/x)
// donde arctan(y/x) en el cuarto cudrante siempre da negativo o cero porque que a todos los puntos les asigno
// un espacio que miro pero en realidad no hay una biyeccion del eje de coordenadas y mi plano (r,tita) pues
// me quedan infinitos puntos afuera (6.2814, 2 pi)
    return losClusters;
}

vector<Adyacencias> kruskal(vector<int> cluster, Grafo g) {
    vector<Adyacencias> adyecienciasDelCluster;
    for (int i = 0; i < cluster.size(); ++i) {
        for (int j = 0; j < cluster.size(); ++j) {
            if (cluster[i] != cluster[j]) {
                Adyacencias ady;
                ady.desdeDonde = cluster[i];
                ady.hastaDonde = cluster[j];
                ady.peso = g.MatrizDeAdyacencia()[cluster[i]][cluster[j]];
                adyecienciasDelCluster.push_back(ady);
            }
        }
    }
    sort(adyecienciasDelCluster.begin(), adyecienciasDelCluster.end(), ordK);
    vector<bool> pasePor(g.CantidadDeElementos(), false);
    vector<Adyacencias> res;
    pasePor[adyecienciasDelCluster[0].desdeDonde] = true;
    pasePor[adyecienciasDelCluster[0].hastaDonde] = true;
    res.push_back(adyecienciasDelCluster[0]);
    for (int k = 1; k < adyecienciasDelCluster.size(); ++k) {
        if (!(pasePor[adyecienciasDelCluster[k].desdeDonde] && pasePor[adyecienciasDelCluster[k].hastaDonde])) {
            pasePor[adyecienciasDelCluster[k].desdeDonde] = true;
            pasePor[adyecienciasDelCluster[k].hastaDonde] = true;
            res.push_back(adyecienciasDelCluster[k]);
        }
    }
    return res;
}

void TSP(vector<int> cluster, Resultados &res, Grafo g) {
    if (cluster.size() > 1) {
        vector<Adyacencias> AGM = kruskal(cluster, g);
        ruta rutaDeEsteCluster;
        rutaDeEsteCluster.push_back(g.Deposito()+1);
        rutaDeEsteCluster.push_back(AGM[0].desdeDonde + 1);
        res.funcionObjetivo += g.MatrizDeAdyacencia()[g.Deposito()][AGM[0].desdeDonde];
        vector<bool> pasePor(g.CantidadDeElementos(), true);
        for (int i = 0; i < cluster.size(); i++) {
            pasePor[cluster[i]] = false;
        }
        pasePor[AGM[0].desdeDonde] = true;
        int estoyEn = AGM[0].desdeDonde;
        int nodosQueRecorri = 1;
        while (nodosQueRecorri < cluster.size()) { // mientras haya nodos que pasar
            bool encontreUnoPorCualPasar = false;
            for (int i = 0; i < AGM.size(); ++i) {
                if (AGM[i].desdeDonde == estoyEn && !pasePor[AGM[i].hastaDonde]) {
                    rutaDeEsteCluster.push_back(AGM[i].hastaDonde + 1);
                    res.funcionObjetivo += g.MatrizDeAdyacencia()[estoyEn][AGM[i].hastaDonde];
                    estoyEn = AGM[i].hastaDonde;
                    pasePor[AGM[i].hastaDonde] = true;
                    encontreUnoPorCualPasar = true;
                    nodosQueRecorri++;
                }
            }
            if (!encontreUnoPorCualPasar) {
                for (int i = 0; i < pasePor.size(); ++i) {
                    if (pasePor[i] == false) {
                        rutaDeEsteCluster.push_back(i + 1);
                        res.funcionObjetivo += g.MatrizDeAdyacencia()[estoyEn][i];
                        estoyEn = i;
                        pasePor[i] = true;
                        nodosQueRecorri++;
                        break;
                    }
                }
            }
        }
        res.funcionObjetivo += g.MatrizDeAdyacencia()[estoyEn][g.Deposito()];
        rutaDeEsteCluster.push_back(g.Deposito()+1);
        res.rutas.push_back(rutaDeEsteCluster);
    } else {
        ruta rutaDeEsteCluster;
        rutaDeEsteCluster.push_back(g.Deposito()+1);
        rutaDeEsteCluster.push_back(cluster[0] + 1);
        rutaDeEsteCluster.push_back(g.Deposito()+1);
        res.funcionObjetivo += 2 * g.MatrizDeAdyacencia()[cluster[0]][g.Deposito()];
        res.rutas.push_back(rutaDeEsteCluster);

    }
}