#ifndef TP_III_UTILS_H
#define TP_III_UTILS_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <math.h>
#include <vector>
#include <chrono>
#include <limits>
#include <queue>
#include <tuple>
#include <utility>
#include <tgmath.h>
#include <bits/stdc++.h>

using namespace std;

#include "tipos.h"
#include "grafo.h"

//CLUSTER

int minKey(float key[], bool visited[], int V);

void prim(Grafo& g);

void promediosAux(Grafo &g, vector<int> adj[],int n,int &d, float &suma, int &nodos, bool visited[]);

void calculoPromedios(Grafo g, vector<float> average[], vector<int> adj[], int d_or);

void desviosAux(Grafo &g, vector<int> adj[],int n,int &d, float &suma, float averageN, int &nodos, bool visited[]);

void calculoDesvios(Grafo g, vector<float> deviations[], vector<float> average[], vector<int> adj[], int d_or);

bool consistent(arista edge, vector<int> adj[], float k, float t, vector<float> average[], vector<float> deviations[]);

void DFS(Grafo &g, int v, cluster &c, bool visited[], vector<int> adj[]);

void armarClusters(Grafo &g, vector<cluster> &clusters, vector<int> adj[]);

Resultados Cluster(Grafo &g);

//SWEEP

bool ord(coordP a, coordP b);

bool ordK(Adyacencias a, Adyacencias b);

vector<coordP> CoordenadasPolares(Grafo &g);

clusters ClusterSweep (Grafo g);

vector<Adyacencias> kruskal(vector<int> cluster,Grafo g);

void TSP (vector<int> cluster, Resultados &res, Grafo g);

Resultados Sweep(Grafo &g);

//GOLOSA

matriz FloydWarshall(matriz grafo);

int nodoMasCercano(matriz Floyd, vectorDeMarcas yaPase, int nodo, int deposito);

Resultados Golosa(Grafo &g);

//SIMANN

bool esMasOptimo(Resultados res1, Resultados res2);

Resultados dameVecino(Resultados solucion, Grafo g);

float energia(Resultados res);

float probabilidadS(float sPrima, float s, float temperatura, bool &control, float &elAnterior);

void enfriar( float &temperatura, float funcionObjetivo, int i, int cuantoRecorro);

Resultados SimAnn(Grafo &g, int cuantoRecorro);

//SAVINGS

Resultados Savings(Grafo &g);

vector<Ahorro> calcularAhorros(Grafo g);

bool ordAhorros(Ahorro a, Ahorro b);

vector<ruta> calcularRutas(vector<Ahorro> ahorros, Grafo g);

void unirRutas(vector<ruta> &rutas, int j, int k, Ahorro ahorro);

int costoDeRuta(const ruta &ruta, Grafo g);

void sumarUno(vector<ruta> &rutas);

void agregarDeposito(vector<ruta> &rutas, Grafo g);

float funcionObj(const vector<ruta>& ruta, Grafo g);
#endif //TP_III_UTILS_H