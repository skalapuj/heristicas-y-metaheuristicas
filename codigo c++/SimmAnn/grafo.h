#ifndef TP_III_GRAFO_H
#define TP_III_GRAFO_H

using namespace std;

class Grafo {
public:

//  string _nombre;
    int _cantidadDePuntos; //*
    int _capacidad; //*
    vector<coord> _puntosEnElMapa; //*
    int _indexDeposito; //*
    coord _deposito; //*
    vector<vector<float>> _grafo; //*
    vector<arista> _aristas; //*
    vector<int> _costoDelPunto; //*
    vector<float> _distanciasAlDeposito;
    string _nombre;


    Grafo();

    int CapacidadDelVehiculo();

    int CantidadDeElementos();

    vector<int> CostoPorPunto();

    int CostoDePasarPorElNodo(int i);

    //string NombreDeInstancia();
    vector<vector<float>> MatrizDeAdyacencia();

    int Deposito();

    vector<coord> puntosEnElMapa();

    void quitarDeposito();

};


#endif //TP_III_GRAFO_H