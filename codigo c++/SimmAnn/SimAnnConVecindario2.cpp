#include "utils.h"

using namespace std;

Resultados SimAnn(Grafo &g, int cuantoRecorro, string algoritmo) {
    Grafo original=g;
    Resultados res1;
    if (algoritmo == "sweep"){
        res1=Sweep(g);
    }
    if (algoritmo == "savings"){
        res1=Savings(g);
    }
    if (algoritmo == "cluster"){
        res1=Cluster(g);
    }
    if (algoritmo == "golosa"){
        res1=Golosa(g);
    }
    g= original;
    float temperaturaMaxima = 0;
    for (int j = 0; j < g.CantidadDeElementos(); ++j) {
        Resultados vecino = dameVecino(res1, g);
        if (vecino.funcionObjetivo > temperaturaMaxima) {
            temperaturaMaxima = vecino.funcionObjetivo;
        }
    }
    Resultados resultadoOptimo = res1;
    Resultados resultadoProvisorio = resultadoOptimo;
    float temperatura = temperaturaMaxima;
    bool control = true;
    float elValorAtnerior=1;
    float proba=0;
    for (int i = 0; i < cuantoRecorro; ++i) {
        Resultados algunVecino = dameVecino(resultadoProvisorio, g);
        float numeroRandom = float((rand() % 100000)) / 100000.0;
        float energiaDeAlgunVecino = energia(algunVecino);
        float energiaActual = energia(resultadoProvisorio);
        proba = probabilidadS(energiaDeAlgunVecino, energiaActual, temperatura, control, elValorAtnerior);
        enfriar(temperatura, resultadoOptimo.funcionObjetivo, i, cuantoRecorro);
        if (proba >= numeroRandom) {
            resultadoProvisorio = algunVecino;
        }
        if (esMasOptimo(resultadoProvisorio, resultadoOptimo)) {
            resultadoOptimo = resultadoProvisorio;
            enfriar(temperatura, resultadoOptimo.funcionObjetivo, i, cuantoRecorro);
        }
    }
    return resultadoOptimo;
}

//FUNCIONES

bool esMasOptimo(Resultados res1, Resultados res2) {
    if (res1.funcionObjetivo < res2.funcionObjetivo) {
        return true;
    } else if (res1.funcionObjetivo > res2.funcionObjetivo) {
        return false;
    } else {
        return res1.rutas.size() < res2.rutas.size();
    }
}


float sumarRutas(vector<ruta> rutas,Grafo g){
    float suma=0;
    for (int i = 0; i < rutas.size(); ++i) {
        for (int j = 0; j < rutas[i].size()-1; ++j) {
            suma = suma + g.MatrizDeAdyacencia()[rutas[i][j]-1][rutas[i][j+1]-1];
        }
    }
    return suma;
}

Resultados dameVecino(Resultados solucion, Grafo g) {
    Resultados res = solucion;
    int rutaQueVoyACambiar = rand() % (solucion.cantidadDeVehiculos);
    int rutaQueVoyACambiar2= rand() % (solucion.cantidadDeVehiculos);
    int nodoA = rand() % (solucion.rutas[rutaQueVoyACambiar2].size() - 2) + 1;
    int nodoB = rand() % (solucion.rutas[rutaQueVoyACambiar].size() - 2) + 1;
    swap(res.rutas[rutaQueVoyACambiar2][nodoA], res.rutas[rutaQueVoyACambiar][nodoB]);
    res.cantidadDeVehiculos = solucion.cantidadDeVehiculos;
    res.rutas;
    res.funcionObjetivo=sumarRutas(res.rutas, g);
    return res;
}

float energia(Resultados res) {
    return float(res.cantidadDeVehiculos) / res.funcionObjetivo;
}

float probabilidadS(float sPrima, float s, float temperatura, bool &control, float &elAnterior) {
    float res;
    if (sPrima < s && control) {
        elAnterior=1;
        res= 1;
    } else {
        res = exp(-(sPrima - s + 0.001) / temperatura);
        if (res >= 1) {
            res = elAnterior;
        }
        if (res > elAnterior){
            res= elAnterior;
        }
        elAnterior=res;
        control = false;
    }
    return res;
}

void enfriar(float &temperatura, float funcionObjetivo, int i, int cuantoRecorro) {
    temperatura = temperatura / (1.009);
}
