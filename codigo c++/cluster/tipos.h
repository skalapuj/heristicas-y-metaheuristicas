#ifndef TP_III_TIPOS_H
#define TP_III_TIPOS_H

using namespace std;

typedef vector<vector<float>>  matriz;
typedef vector<vector<int>>  clusters;
typedef vector<bool> vectorDeMarcas;
typedef vector<int> ruta;

struct coord {
  int x;
  int y;
};

struct coordP {
  float r;
  float t;
  int id;
};

struct arista {
  int A;
  int B;
  float peso;
};

struct cluster {
  int masCercano;
  int costo;
  float longitud;
  vector<int> recorrido;
};

struct Resultados {
  int cantidadDeVehiculos = 0;
  vector<ruta> rutas;
  float funcionObjetivo;
};

struct Adyacencias {
  int desdeDonde;
  int hastaDonde;
  float peso;
};

struct Ahorro {
    float ahorro;
    int desde;
    int hasta;
};

#endif //TP_III_TIPOS_H