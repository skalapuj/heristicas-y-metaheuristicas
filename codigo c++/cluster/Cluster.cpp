#include "utils.h"

using namespace std;

Resultados Cluster(Grafo &g) {

  g.quitarDeposito();

  Resultados res;

  float k = 5;
  float t = 5;
  int d = 3;

  //hacer AGM
  prim(g);

  //armar lista de adyacencias
  vector<int> adj[g._cantidadDePuntos];
  for (arista &a : g._aristas) {
    adj[a.A-1].push_back(a.B-1);
    adj[a.B-1].push_back(a.A-1);
  }

  //calculo el promedio
  vector<float> average[g._cantidadDePuntos];
  vector<float> deviations[g._cantidadDePuntos];
  
  calculoPromedios(g, average,adj, d);
  calculoDesvios(g, deviations,average, adj, d);

  //recorrer ejes y eliminar inconsistentes
  vector<arista> consistent_edges;
  vector<int> consistent_adj[g._cantidadDePuntos];
  for (arista a : g._aristas) {
    if (consistent(a, adj, k, t, average, deviations)) {
      consistent_adj[a.A-1].push_back(a.B-1);
      consistent_adj[a.B-1].push_back(a.A-1);
      consistent_edges.push_back(a);
    }
  }

  //armar lista de clusters con su TSP
  vector<cluster> clusters;
  armarClusters(g, clusters, adj);

  //armar el resultado

  res.cantidadDeVehiculos = clusters.size();

  float longitudTotal = 0;

  for (int i = 0; i < clusters.size(); i++) {
    vector<int> ruta_i;
    float primerArista = g._distanciasAlDeposito[clusters[i].recorrido[clusters[i].masCercano]];
    float ultimaArista = g._distanciasAlDeposito[clusters[i].recorrido[(clusters[i].masCercano-1) % clusters[i].recorrido.size()]];
    longitudTotal += clusters[i].longitud + primerArista + ultimaArista;
    ruta_i.push_back(1);
    for (int j = 0; j < clusters[i].recorrido.size(); j++) {
      int p = clusters[i].recorrido[(j + clusters[i].masCercano) % clusters[i].recorrido.size()] + 1;
      ruta_i.push_back(p);
    }
    ruta_i.push_back(1);
    res.rutas.push_back(ruta_i);
  }

  res.funcionObjetivo = longitudTotal;

  return res;
}

//FUNCIONES

int minKey(float key[], bool visited[], int V) {
  // inicializar min para comparar
  float min = INT_MAX, min_index;

  for (int v = 0; v < V; v++) {
    if (visited[v] == false && key[v] < min)
      min = key[v], min_index = v;
  }

  return min_index;
}

void prim(Grafo& g) {  
  int V = g._cantidadDePuntos;
  int parent[V];

  // key va a ser la distancia de cada nodo al arbol
  float key[V];

  bool visited[V];

  // todas las keys empiezan como infinito
  for (int i = 0; i < V; i++) {
    key[i] = INT_MAX, visited[i] = false;
  }

  // Para que el vertice 0 sea elegido primero
  key[0] = 0;
  parent[0] = -1; // no tiene padre

  // queremos agregar v - 1 aristas
  for (int count = 0; count < V - 1; count++) {  
    // Se elige el que este a menor distancia
    int u = minKey(key, visited, V);

    // se lo agrega al AGM
    visited[u] = true;

    // actualizar padres y distancias
    for (int v = 0; v < V; v++) {
      // solo consideramos los adyacentes al que agregamos antes
      if (g._grafo[u][v] && visited[v] == false && g._grafo[u][v] < key[v])
        parent[v] = u, key[v] = g._grafo[u][v];
    }
  }

  vector<arista> aristas;
  //output del algoritmo
  for(int i = 1; i < V; ++i) {
    arista a;
    a.A=i+1;
    a.B=parent[i]+1;
    a.peso= g._grafo[i][parent[i]];

    aristas.push_back(a);
  }

  g._aristas=aristas;
}

void promediosAux(Grafo &g, vector<int> adj[],int n,int &d, float &suma, int &nodos, bool visited[]) {
  if (d == 0) return;

  visited[n] = true;
  
  for (int m = 0; m < adj[n].size(); m++) {
    int next = adj[n][m];
    if(visited[next] == false) {
      suma += g._grafo[n][next-1];
     
      nodos++;
      d--;

      promediosAux(g,adj,next,d,suma,nodos, visited);
    }
  }  
}

void calculoPromedios(Grafo g, vector<float> average[], vector<int> adj[], int d_or) {
  for (int i = 0; i < g._cantidadDePuntos; i++) {
    vector<float> a(g._cantidadDePuntos,0);
    average[i] = a;

    for (int n = 0; n < adj[i].size(); n++) {
      int d = d_or;
      float suma = 0;
      int nodos = 0;
      int id = adj[i][n];
      bool visited[g._cantidadDePuntos] = {0};
      visited[id] = true;
      promediosAux(g, adj, i, d, suma, nodos, visited);

      if (nodos != 0 && suma !=0) {
        average[i][id]= suma / nodos;
      }
    }
  }
}

void desviosAux(Grafo &g, vector<int> adj[],int n,int &d, float &suma, float averageN, int &nodos, bool visited[]) {
  if (d == 0) return;

  visited[n] = true;
  
  for (int m = 0; m < adj[n].size(); m++) {
    int next = adj[n][m];
    if(visited[next] == false) {
      float dif = g._grafo[n][next-1] - averageN;
      suma += pow(dif, 2);
      nodos++;
      d--;

      desviosAux(g, adj,next,d,suma, averageN, nodos, visited);
    }
  }  
}

void calculoDesvios(Grafo g, vector<float> deviations[], vector<float> average[], vector<int> adj[], int d_or) {
  for (int i = 0; i < g._cantidadDePuntos; i++) {
    vector<float> a(g._cantidadDePuntos,0);
    deviations[i] = a;

    for (int n = 0; n < adj[i].size(); n++) {
      int d = d_or;
      float suma = 0;
      int nodos = 0;
      int id = adj[i][n];
      float averageN = average[i][id];
      bool visited[g._cantidadDePuntos] = {0};
      visited[id] = true;
      desviosAux(g, adj, i, d, suma, averageN, nodos, visited);

      if (nodos - 1 != 0 && suma !=0 ) {
        deviations[i][id]= sqrt(suma / (nodos - 1));
      }
    }
  }
}

bool consistent(arista edge, vector<int> adj[], float k, float t, vector<float> average[], vector<float> deviations[]) {
  
  float averageA = average[edge.A][edge.B]; //promedio nodo A sin eje AB
  float averageB =  average[edge.B][edge.A]; //promedio nodo B sin eje AB

  float deviationA = deviations[edge.A][edge.B]; //desviacion nodo A sin eje AB
  float deviationB =  deviations[edge.B][edge.A]; //desviacion nodo B sin eje AB

  if (edge.peso > averageA + k*deviationA) {
    if (edge.peso > averageB + k*deviationB) return false;
  }
  
  if((edge.peso / averageA) > t && (edge.peso / averageB) > t) return false;

  return true;
}

void DFS(Grafo &g, int v, cluster &c, bool visited[], vector<int> adj[]) {
  visited[v] = true;
      
    // recursion sobre los vecinos 
  for(int j = 0; j < adj[v].size(); j++) {
    int u = adj[v][j];
    if(!visited[u]) {
      if (c.costo + g._costoDelPunto[u] > g._capacidad) return;
      c.costo += g._costoDelPunto[u];
      c.recorrido.push_back(u+1);
      c.longitud += g._grafo[v][u];
      if (g._distanciasAlDeposito[u] < g._distanciasAlDeposito[c.recorrido[c.masCercano]]) c.masCercano = c.recorrido.size()-1;
      DFS(g, u, c, visited, adj);
    }
  }
}

void armarClusters(Grafo &g, vector<cluster> &clusters, vector<int> adj[]){
  bool *visited = new bool[g._cantidadDePuntos]; 
  for(int v = 0; v < g._cantidadDePuntos; v++) {
    visited[v] = false; 
  }

  for (int v=0; v<g._cantidadDePuntos; v++) { 
    if (visited[v] == false) { 
      cluster c;
      c.masCercano = 0;
      c.recorrido.push_back(v+1);
      c.costo = g._costoDelPunto[v];
      c.longitud = 0;
      // recorro todos los alcanzables y los agrego a c
      DFS(g, v, c, visited, adj);

      clusters.push_back(c);
    } 
  } 
}