#include "utils.h"

using namespace std;

Resultados Golosa(Grafo &g){
  Resultados res;
  matriz Floyd = FloydWarshall(g.MatrizDeAdyacencia());
  vectorDeMarcas yaPasePor(g.CantidadDeElementos(),false);
  int control = 0;
  res.funcionObjetivo = 0;
  while (control < g.CantidadDeElementos() - 1){
    res.cantidadDeVehiculos++;
    int capacidadTemporal = g.CapacidadDelVehiculo();
    int nodoActual = g.Deposito();
    ruta rutaDeEste;
    rutaDeEste.push_back(g.Deposito()+1);
    while(control < g.CantidadDeElementos() - 1 && capacidadTemporal>= g.CostoDePasarPorElNodo(nodoMasCercano(Floyd, yaPasePor, nodoActual, g.Deposito()))){
      nodoActual = nodoMasCercano(Floyd, yaPasePor, nodoActual, g.Deposito());
      yaPasePor[nodoActual] = true;
      control++;
      int ultimoAgregado = rutaDeEste[rutaDeEste.size()-1] - 1;
      rutaDeEste.push_back(nodoActual+1);
      res.funcionObjetivo += g.MatrizDeAdyacencia()[ultimoAgregado][nodoActual];
      capacidadTemporal -= g.CostoDePasarPorElNodo(nodoActual);
    }
    res.funcionObjetivo += g.MatrizDeAdyacencia()[nodoActual][g.Deposito()];
    rutaDeEste.push_back(g.Deposito()+1);
    res.rutas.push_back(rutaDeEste);
  }
  return res;
}

//FUNCIONES

matriz FloydWarshall(matriz grafo){
  matriz Floyd= grafo;
  for (int j = 0; j < Floyd.size(); ++j) {
    for (int k = 0; k < Floyd.size(); ++k) {
      if(j==k){
        continue;
      }
      for (int l = 0; l < Floyd.size(); ++l) {
        if (j==l){
          continue;
        }
        float suma = Floyd[k][j] +  Floyd[j][l];
        if(Floyd[k][l] > suma){
          Floyd[k][l] = suma;
        }
      }
    }
  }
  return Floyd;
}

int nodoMasCercano(matriz Floyd, vectorDeMarcas yaPase, int nodo, int deposito){
  int i = 0;
  while( i == deposito || i == nodo || yaPase[i]) {
    i++;
  }
  float minimo = Floyd[nodo][i];
  int min_index = i;
  while( i < Floyd.size()) {
    if (Floyd[nodo][i]<minimo){
      if (!(i== deposito || i == nodo || yaPase[i])){
        minimo=Floyd[nodo][i];
        min_index = i;
      }
    }
    i++;
  }
  return min_index;
}