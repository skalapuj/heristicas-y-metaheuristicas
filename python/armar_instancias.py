import math
import sys
import datetime
import random
import glob, os
import subprocess
from subprocess import Popen, PIPE

for file in glob.glob("../Vrp-Set-XXL/*.vrp"):
    #A todo le agrego lstrip y rstrip porque le saca los espacios de los costados
    #Abro el archivo que voy a limpiar
    archivo=open(file,'rb')

    #Leo la primer linea
    lista = archivo.readlines()
    
    #edge_type = ((lista[4]).decode("utf-8")).split(" ",3)
    #if edge_type[2].rsplit() != "EXPLICIT":
    instancia = ((lista[0]).decode("utf-8")).split(" ",3)
    nombre = instancia[2].rstrip()
    
    #Creo el archivo que voy a escribir
    file_name = "./instancia_%s.txt" % (nombre)
    output_file = open(file_name, 'w+')
    
    #Leo las lineas que faltan
    dimension = ((lista[3]).decode("utf-8")).split(" ",3)
    capacidad = ((lista[5]).decode("utf-8")).split(" ",3)
	    
    #Pongo los datos en el nuevo archivo
    output_file.write((dimension[2].lstrip().rstrip())+'\n')
    output_file.write((capacidad[2].lstrip().rstrip())+'\n')
    
    #Leo las coordenadas de los nodos y pongo 
    cantDeNodos = int(dimension[2])
    for nodo in range(7,7+cantDeNodos):
	datos = (lista[nodo].decode("utf-8")).split(" ",3)
	if len(datos)==4:
	    output_file.write(datos[1].rstrip().lstrip()+'\n')
	    output_file.write(datos[2].rstrip()+'\n')
	    output_file.write(datos[3].rstrip().lstrip()+'\n')  
	else:
	    output_file.write(datos[0].rstrip().lstrip()+'\n')
	    output_file.write(datos[1].rstrip()+'\n')
	    output_file.write(datos[2].rstrip().lstrip()+'\n')
    for i in range(8+cantDeNodos,8+2*cantDeNodos):
	datos = (lista[i].decode("utf-8")).split(" ",2)
	output_file.write(datos[0].lstrip().rstrip()+'\n')
	output_file.write(datos[1].lstrip().rstrip()+'\n')
    output_file.write((lista[9+2*cantDeNodos].decode("utf-8")).lstrip().rstrip())
